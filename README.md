# Get A Feed

Welcome to the issue tracking platform for Get A Feed beta testers.

## Reporting bugs

To report a bug, please navigate to the Issues tab, then create an issue with type bug.

## Requesting new features

To request a feature, please navigate to the Issues tab, then create an issue with type feature.

